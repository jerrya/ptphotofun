# Website for PT Photo Fun a Photo booth rental service

For my niece's Bat Mitzvah, I created a photo booth. And now when
friends ask for their events, I operate a rental photobooth in the
local community. Well it needed a website, didn't it? This is the
website.

It can be found at [PT Photofun](http://ptphotofun.com)

This website is based on the Freelancer Bootstrap theme found at Start Bootstrap.

