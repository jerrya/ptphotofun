#+TODO: TODO IN-PROGRESS WAITING DONE
* Main

** DONE Photo Booth

PT Photo Fun rents photo booths for any event. Liven up your party with laughs & wonderful pictures. Your guests will have lots of fun posing and laughing while making your party even more memorable. When it's over, your photos will be given to you for you to enjoy and share. When you rent from PT Photo Fun our team will work with you to create photo layouts customized to your event and then set up, operate and bring down the photo booth when your party ends.

Photo Booths are great at
Weddings
Graduations Parties
Bar / Bat Mitzvahs
Birthdays
Proms
Corporate Events
Anniversary
Dances
Parties
Church Events
School Events
Reunions
Grand Openings
Fund Raisers
Bachelorette Party
Everything Else!

** How It Works

*** Open Photo Booth

Open Set Photo Booth

Open set allows more people to fit in each shot, 
uses better lighting, and looks amazing.

**** Table or Standing (table is better when there is a mix of kids and adults)

*** Slideshow

*** Picture quality printers
*** Specialized Layouts
**** Choice of
**** 4x6 
**** 4 strips
**** dup strips
**** Combinations

*** Custom Backdrops
**** Backdrops for your occasion

**** Our Props or Yours or Both

** DONE Pricing

$300 for 3 hours
First 200 prints free
Custom layouts free
Pictures delivered at end of event on a USB stick

** Reviews

** Contact

* Footer

* Ad Campaigns
** PT Leader
** Google AdWords
** Facebook
** PT Wedding Photogs
